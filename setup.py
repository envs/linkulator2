from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="linkulator",
    version="2.0.0",
    author="cmccabe",
    author_email="rawtext.club",
    description="Linkulator is a command line link aggregator; somewhat like news.ycombinator.com, or lobste.rs, but simpler and run from the command line.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://tildegit.org/cmccabe/linkulator2",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX",
    ],
    python_requires=">=3.6",
)
