#!/usr/bin/env python3
"""Tests for Linkulator views"""

import unittest
from unittest.mock import patch, call
import linkulator


class TestPrintSearchResults(unittest.TestCase):
    """Tests covering the print_search_results function"""

    @patch("builtins.print")
    def test_print_search_results(self, mock_print):
        """tests that the search results are printed correctly"""
        test_keyword = "keyword"
        test_search_results = [
            (66, "keyword", "1576461366.5580268", "", "c", "c", "c"),
            (65, "poster6", "1576461367.5580268", "", "keyword", "c", "c"),
            (64, "poster7", "1576461368.5580268", "", "c", "keyword", "c"),
            (63, "poster8", "1576461369.5580268", "", "c", "c", "keyword"),
        ]
        test_print_calls = [
            call(
                "\nShowing results for keyword\n\n ID#  DATE           AUTHOR      DESC         "
            ),
            call("   1  2019-12-16     keyword     c            "),
            call("   2  2019-12-16     poster6     c            "),
            call("   3  2019-12-16     poster7     c            "),
            call("   4  2019-12-16     poster8     keyword      "),
            call(""),
        ]

        linkulator.print_search_results(test_keyword, test_search_results)

        self.assertEqual(
            mock_print.call_count, 6
        )  # one count for title, 4 for the items and a blank line for formatting

        self.assertListEqual(test_print_calls, mock_print.call_args_list)
