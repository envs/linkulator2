"""unit tests for the data module"""
import unittest
import unittest.mock
from unittest.mock import mock_open
from time import time
import data


class TestDataHelperFunctions(unittest.TestCase):
    """Tests that cover helper functions, mostly handling string validation"""

    def test_wash_line(self):
        """tests the data.wash_line function"""
        teststrings = [
            {"Test": "A line of text", "Result": "A line of text"},
            {"Test": "A line of text\n", "Result": "A line of text"},
            {"Test": "\033[95mPink text\033[0m", "Result": "Pink text"},
            {"Test": "🚅\t\n", "Result": "🚅"},
            {
                "Test": "gemini://gemini.circumlunar.space",
                "Result": "gemini://gemini.circumlunar.space",
            },
        ]
        for line in teststrings:
            self.assertEqual(data.wash_line(line["Test"]), line["Result"])

    def test_is_well_formed_line(self):
        """ tests the data.is_well_formed_line function"""
        teststrings = [
            {"Test": "A line of text", "Result": False},
            {"Test": "1 Pipe |", "Result": False},
            {"Test": "4 Pipes ||||", "Result": True},
            {"Test": "5 Pipes |||||", "Result": False},
            {"Test": "|P|I|P|E|H|E|A|V|E|N||||||||||", "Result": False},
        ]
        for line in teststrings:
            self.assertEqual(data.is_well_formed_line(line["Test"]), line["Result"])

    def test_is_valid_time(self):
        """tests the data.is_valid_time function"""
        teststrings = [
            {"Test": "946645140.0", "Result": True},  # 1999
            {"Test": str(time() + 10), "Result": False},
        ]
        for line in teststrings:
            self.assertEqual(data.is_valid_time(line["Test"]), line["Result"])

    def test_process(self):
        """tests the data.process function"""
        # wash line
        self.assertEqual(
            data.process("\t|\033[95mPink text\033[0m|||\n", ""),
            ["", "", "Pink text", "", "", ""],
        )

        # is well formed line
        with self.assertRaises(ValueError, msg="Not a well formed line"):
            data.process("|||||\n", "")

        # is valid date
        with self.assertRaises(ValueError, msg="Invalid date"):
            data.process("{}||||".format(str(time() + 10)), "")

        # real data tests
        teststrings_input = [
            "1576123922.106229|user+1576032469.7391915|||a new reply\n",
            "1576137798.4647715|user+1576032469.7391915|||Here is another new reply\n",
            "575968281.7483418||tildes|gopher://tilde.town|Tilde Town\n",
            "1575969313.8278663||tildes|http://tilde.team|Tilde Team\n",
        ]
        teststrings_output = [
            [
                "username0",
                "1576123922.106229",
                "user+1576032469.7391915",
                "",
                "",
                "a new reply",
            ],
            [
                "username1",
                "1576137798.4647715",
                "user+1576032469.7391915",
                "",
                "",
                "Here is another new reply",
            ],
            [
                "username2",
                "575968281.7483418",
                "",
                "tildes",
                "gopher://tilde.town",
                "Tilde Town",
            ],
            [
                "username3",
                "1575969313.8278663",
                "",
                "tildes",
                "http://tilde.team",
                "Tilde Team",
            ],
        ]
        for i, item in enumerate(teststrings_input):
            self.assertEqual(
                data.process(item, "username{}".format(i)), teststrings_output[i]
            )

        def test_get_parent_record(self):
            # test that an empty parent_id throws a value error
            test_link_data = []
            parent_id = ""

            with self.assertRaises(ValueError):
                data.get_parent_record(parent_id, test_link_data)

            # confirm the function returns a valid parent record
            test_parent_record = [
                1,
                "testuser",
                "1000",
                "",
                "test_category",
                "test_url",
                "test_title",
            ]
            test_link_data = (
                [
                    [
                        1,
                        "testuser",
                        "1000",
                        "",
                        "test_category",
                        "test_url",
                        "test_title",
                    ]
                ],
            )
            parent_id = 1000

            self.assertListEqual(
                test_parent_record, data.get_parent_record(parent_id, test_link_data)
            )

            # confirm key error raised if no results found

            parent_id = 69

            with self.assertRaises(KeyError):
                data.get_parent_record(parent_id, test_link_data)


class TestLinkDataSearch(unittest.TestCase):
    @unittest.mock.patch.object(data.LinkData, "get")
    def test_search_exceptions(self, mock_get):
        """ensures exceptions are raised"""
        link_data = data.LinkData()
        mock_get.assert_called()
        with self.assertRaises(ValueError):
            link_data.search("")

    @unittest.mock.patch.object(data.LinkData, "get")
    def test_search(self, mock_get):
        """tests search function"""
        link_data = data.LinkData()
        mock_get.assert_called()

        link_data.link_data = [
            [
                "",
                "user1",
                "1576486443.8539028",
                "poster1+1576289662.7914467",
                "",
                "",
                "this comment contains a keyword regarding the keyword website",
            ],
            [
                "",
                "user2",
                "1576486440.65404",
                "poster1+1576289662.7914467",
                "",
                "",
                "this is a reply to the site but does not contain the word",
            ],
            [
                70,
                "poster1",
                "1576289662.7914467",
                "",
                "keyword",
                "http://keyword.com",
                "the keyword website",
            ],
            [
                69,
                "poster2",
                "1576462584.2307518",
                "",
                "keyword",
                "gemini://keyword",
                "keyword site with no replies",
            ],
            [68, "poster3", "1576462007.9509487", "", "a", "a", "key word"],
            [67, "poster4", "1576461366.5580268", "", "b", "b", "key.word"],
            [
                "",
                "user3",
                "1576376868.284987",
                "poster3+1576376644.2783155",
                "",
                "",
                "this is an orphaned reply but it contains the keyword",
            ],
            [66, "keyword", "1576461366.5580261", "", "c", "c", "c"],
            [65, "poster6", "1576461367.5580268", "", "keyword", "c", "c"],
            [64, "poster7", "1576461368.5580268", "", "c", "keyword", "c"],
            [63, "poster8", "1576461369.5580268", "", "c", "c", "keyword"],
            [62, "poster9", "1576461370.5580268", "", "c", "c", "ssskeywordsubstring"],
            [61, "poster0", "1576461371.5580268", "", "c", "c", "KEYWORD capital"],
            [60, "poste99", "1576461372.5580268", "", "c", "c", "c"],
            [
                "",
                "user99",
                "1576461372.6680268",
                "poste99+1576461372.5580268",
                "",
                "",
                "the post doesn't contain the keyword but this reply does",
            ],
        ]

        test_results = [
            (
                70,
                "poster1",
                "1576289662.7914467",
                "",
                "keyword",
                "http://keyword.com",
                "the keyword website",
            ),
            (
                69,
                "poster2",
                "1576462584.2307518",
                "",
                "keyword",
                "gemini://keyword",
                "keyword site with no replies",
            ),
            (66, "keyword", "1576461366.5580261", "", "c", "c", "c"),
            (65, "poster6", "1576461367.5580268", "", "keyword", "c", "c"),
            (64, "poster7", "1576461368.5580268", "", "c", "keyword", "c"),
            (63, "poster8", "1576461369.5580268", "", "c", "c", "keyword"),
            (62, "poster9", "1576461370.5580268", "", "c", "c", "ssskeywordsubstring"),
            (61, "poster0", "1576461371.5580268", "", "c", "c", "KEYWORD capital"),
            (60, "poste99", "1576461372.5580268", "", "c", "c", "c"),
        ]

        self.assertEqual(link_data.search("keyword"), test_results)


class TestAddLink(unittest.TestCase):
    """Tests Add method of LinkData class"""

    @unittest.mock.patch.object(data.LinkData, "get")
    def test_add_to_empty_db(self, mock_get):
        """Tests adding to an empty db as per issue #76"""
        link_data = data.LinkData()
        mock_get.assert_called()

        link_data.link_data = []
        test_record = data.LinkDataRecord(
            username="testuser",
            timestamp="1000",
            category="test_category",
            link_URL="test_url",
            link_title_or_comment="test_title",
        )

        with unittest.mock.patch("builtins.open", mock_open()) as mock_file:

            result = link_data.add(test_record)

            mock_file.assert_called()
            self.assertEqual(1, result)
            self.assertListEqual(
                link_data.link_data,
                [
                    [
                        1,
                        "testuser",
                        "1000",
                        "",
                        "test_category",
                        "test_url",
                        "test_title",
                    ]
                ],
            )


if __name__ == "__main__":
    unittest.main()
