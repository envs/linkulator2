#!/usr/bin/env python3
"""Tests for Linkulator configuration"""

import unittest
import config


class TestConfigDefaults(unittest.TestCase):
    """Tests covering configuration default settings"""

    def test_default_values_set(self):
        """Test for issue reported in #74"""
        # ensure config.USER.lastlogin exists and is a string
        self.assertIsInstance(config.USER.lastlogin, str)
        # ensure config.USER.lastlogin can be compared to string value
        self.assertGreaterEqual(config.USER.lastlogin, "0.0")
