# Linkulator, a link aggregator for the shell

Linkulator is a command line link aggregator; somewhat like [news.ycombinator.com](https://news.ycombinator.com), or [lobste.rs](https://lobste.rs), but simpler and run from the command line.

Designed for multi-user systems like [rawtext.club](https://rawtext.club), each user's links and comments are stored in their home directory, and are compiled for viewing when Linkulator is started.

```shell
user@server:~$ linkulator
 ----------
 LINKULATOR
 ----------

 ID#  New  Category                 
   1   x   non-commercial www (9)
   2   x   how to (1)
   3       data (1)
   4       software (2)
   5       pubnixes (8)
   6       music (2)
   7       test (13)
   8       programming languages (7)
   9       search engines (4)

Enter a category ID, p to post a link, s to search, or q to quit: 
```

## Prerequisites

Linkulator requires Python 3.6.9 or greater. If you use an earlier version, you may get the following error:

```shell
  File "./linkulator.py", line 21
    link_data: list = LinkData.link_data
             ^
SyntaxError: invalid syntax
```

## Installation

Just clone this repo into any location in users' paths.  Each user can have their own copy, in case they want to customize it, or you can install a central version in /usr/local/bin.

On most systems, the default configurations should just work (tm), but follow the config steps below if not.

## Configuration

### Default Path settings

In the case where you have a custom location for user home directories, or want to change where Linkulator stores user data, Linkulator's default settings can be easily changed to support this. The following options are found in the file *config.py* in the class *DefaultPaths*.

#### User home directory location
*String all_homedir_pattern*

Customise user home directory hierarchy. Default is */home/\*/* to support a standard structure like */home/username/*

For example, you may have home directories in a location like */userdata/(first letter)/(username)* i.e. */userdata/a/auser*. The glob pattern would need to be updated to */userdata/i\*/\*/*

#### Data directory name
*String datadir*

Customise the name of the directory where user data is stored. Default is *.linkulator*.

#### Data file
*String datafile*

Customise the name of the file where user links and replies are stored. Default is *linkulator.data*

#### Ignore file
*String ignorefile*

Customise the name of the file where ignored users are stored. Default is *ignore*

#### Settings file
*String settingsfile*

Customise the name of the file where user settings are stored. Default is *linkulatorrc*

### Default User settings
To change the default settings for users of the system, amend the following options found in *config.py* in the class *DefaultUser*

#### Default browser
*String browser*

Customise the command used to execute the default browser used by linkulator. Default is *lynx*

### User settings

Users can select their preferred browser by changing the browser setting in their settings file (default is $HOME/.linkulator/linkulatorrc).

For example, to change from lynx to w3m:

```
[User Settings]
browser = w3m
```

## Contributing

Issues, questions and requests for new features can be reported using the [tildegit issues system](https://tildegit.org/cmccabe/linkulator2/issues).

## License

This software is licensed using GPLv3 - see [LICENSE] for more information.
